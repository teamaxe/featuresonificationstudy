using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Slider = UnityEngine.UI.Slider;
using Toggle = UnityEngine.UI.Toggle;

public class Controller : MonoBehaviour
{
    public Dropdown dropdown;
    public GameObject thumb;
    public Slider contaminationSlider;
    public Slider forceSlider;
    public Slider proximitySlider;

    public AudioSource realSoundSource;
    public AudioSource abstractSoundSource;

    // Start is called before the first frame update
    void Start()
    {
        dropdown.onValueChanged.AddListener(delegate { dropdownChanged(); });
        
        contaminationSlider.onValueChanged.AddListener (delegate { sliderChanged();});
        forceSlider.onValueChanged.AddListener (delegate { sliderChanged();});
        proximitySlider.onValueChanged.AddListener (delegate { sliderChanged();});
        sliderChanged();
    }

    void dropdownChanged()
    {
        realSoundSource.volume = 0.0f;
        abstractSoundSource.volume = 0.0f;

        contaminationSlider.value = 0.5f;
        forceSlider.value = 0.5f;
        proximitySlider.value = 0.5f;
        
        sliderChanged();
    }

    void sliderChanged()
    {
        switch (dropdown.value)
        {
            case 0: //realistic
                realSoundSource.pitch = forceSlider.value * 2.9f + 0.1f;
                realSoundSource.volume = proximitySlider.value;
                realSoundSource.panStereo = contaminationSlider.value * 2f - 1f; 
                break;
            case 1:
                abstractSoundSource.pitch = forceSlider.value * 2.9f + 0.1f;
                abstractSoundSource.volume = proximitySlider.value;
                abstractSoundSource.panStereo = contaminationSlider.value * 2f - 1f; 
                break;
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        updateThumb();
    }
    Vector2 convertNormalisedPointToWorldPoint(Vector2 normalisedPoint)
    {
        float width = 800;
        float height = 800;

        float widthOffset = (Screen.width - width) * 0.5f;
        float heightOffset = (Screen.height - height) * .5f + 50f;

        Vector2 screenPos = new Vector2 (normalisedPoint.x * width + widthOffset, 
            normalisedPoint.y * height + heightOffset);
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(screenPos);
        return worldPos;
    }

    void updateThumb()
    {
        Vector2 thumbPosition = getWorldPoint (contaminationSlider.value, forceSlider.value);
        thumb.transform.position = thumbPosition;

        Vector3 scale;
        scale.x = 0.2f + (3.0f * proximitySlider.value);
        scale.y = 0.2f + (3.0f * proximitySlider.value);
        scale.z = 1.0f;
        thumb.transform.localScale = scale;
    }

    Vector2 getWorldPoint(float xValue, float yValue)
    {
        Vector2 normalisedPoint = new Vector2(xValue, yValue);
        Vector2 point = convertNormalisedPointToWorldPoint (normalisedPoint);
        return point;
    }
}
